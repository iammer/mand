use num::complex::Complex;

#[derive(Debug, Clone, Copy)]
pub struct Scaler {
    min_x: f64,
    min_y: f64,
    scale_x: f64,
    scale_y: f64
}

impl Scaler {
    pub fn new((size_x, size_y): (u32, u32), (min_x, min_y): (f64, f64), (max_x, max_y): (f64, f64)) -> Scaler {
        Scaler {
            min_x: min_x,
            min_y: min_y,
            scale_x: (max_x - min_x) / (size_x as f64),
            scale_y: (max_y - min_y) / (size_y as f64)
        }
    }

    #[inline]
    pub fn scale(&self, (x,y): (u32, u32)) -> (f64, f64) {
        (
            (x as f64) * self.scale_x + self.min_x,
            (y as f64) * self.scale_y + self.min_y
        )
    }

    #[inline]
    pub fn scale_to_complex(&self, p: (u32, u32)) -> Complex<f64> {
        let (re, im) = self.scale(p);
        Complex {
            re: re,
            im: im
        }
    }
}

