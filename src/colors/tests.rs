use super::*;

#[test]
fn hsv() {
    let hsv = rgb_to_hsv([5,10,20]);

    assert_eq!(hsv[0], 170);
    assert_eq!(hsv[1], 191);
    assert_eq!(hsv[2], 20);
}
