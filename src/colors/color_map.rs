extern crate rand;

use self::rand::random;
use std::ops::Mul;

#[inline]
pub fn map_u8(start: i32, end: i32, index: i32, start_c: i32, end_c: i32) -> u8 {
    (((index - start) * (end_c - start_c) / (end - start)) + start_c) as u8
}

fn sqr<N: Mul + Copy>(x: N) -> N::Output {
    x * x
}

#[inline]
pub fn map_u8_s(start: i32, end: i32, index: i32, start_c: i32, end_c: i32) -> u8 {
    ((sqr(index - start) * (end_c - start_c) / sqr(end - start)) + start_c) as u8
}

#[macro_export]
macro_rules! map_color {
    ($f:ident, $start:ident, $end:ident, $index:ident) => {
        [
            $f($start.index as i32, $end.index as i32, $index as i32, $start.color[0] as i32, $end.color[0] as i32),
            $f($start.index as i32, $end.index as i32, $index as i32, $start.color[1] as i32, $end.color[1] as i32),
            $f($start.index as i32, $end.index as i32, $index as i32, $start.color[2] as i32, $end.color[2] as i32)
        ]
    }
}

#[inline]
pub fn rand_color() -> [u8; 3] {
    [ random::<u8>(), random::<u8>(), random::<u8>() ]
}

