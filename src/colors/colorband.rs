use std::cmp::Ordering;

#[derive(Eq, Clone, Copy)]
pub struct ColorBand {
    pub index: usize,
    pub color: [u8; 3]
}

impl PartialEq for ColorBand {
    fn eq(&self, other: &ColorBand) -> bool {
        self.index == other.index
    }
}

impl PartialOrd for ColorBand {
    fn partial_cmp(&self, other: &ColorBand) -> Option<Ordering>{
        self.index.partial_cmp(&other.index)
    }
}

impl Ord for ColorBand {
    fn cmp(&self, other: &ColorBand) -> Ordering {
        self.index.cmp(&other.index)
    }
}

