#[macro_use]
mod color_map;
mod colorband;

#[cfg(test)]
mod tests;

use self::colorband::ColorBand;
use self::color_map::*;

pub struct ColorMapper {
    domain: usize,
    bands: Vec<ColorBand>,
    gradient_fn: u8
}

impl ColorMapper {
    pub fn preset(name: &str, max_iter: usize) -> ColorMapper {
        match name {
            "random" => {
                let mut color_map = ColorMapper::new(rand_color(), rand_color(), max_iter);

                let step = 8;
                let mut i = step;
                while i < max_iter {
                    color_map.add_band(i, rand_color());
                    i+=step;
                }

                color_map
            },
            "preset2" => {
                let mut color_map = ColorMapper::new([0,0,0],[0,0,255],max_iter);

                color_map.add_band_inv(128.0, [255, 0, 0]);
                color_map.add_band_inv(32.0, [0, 255, 255]);
                color_map.add_band_inv(8.0, [255, 255, 0]);
                color_map.add_band_inv(2.0, [0, 255, 0]);

                color_map
            },
            "fixed" => {
                let mut color_map = ColorMapper::new([0,0,0],[0,0,255],max_iter);

                color_map.add_band(16, [255, 0, 0]);
                color_map.add_band(64, [0, 255, 255]);
                color_map.add_band(256, [255, 255, 0]);
                color_map.add_band(1024, [0, 255, 0]);

                color_map
            },
            "sqr" => {
                let mut color_map = ColorMapper::new([0,0,0],[0,0,255],max_iter);
                color_map.gradient_fn = 1;

                color_map.add_band_inv(128.0, [255, 0, 0]);
                color_map.add_band_inv(32.0, [0, 255, 255]);
                color_map.add_band_inv(8.0, [255, 255, 0]);
                color_map.add_band_inv(2.0, [0, 255, 0]);

                color_map
            },
            "mono" => {
                let mut color_map = ColorMapper::new([0,0,0],[255,255,255],max_iter);

                color_map.add_band(max_iter-1,[0,0,0]);

                color_map
            },
            _ => {
                let mut color_map = ColorMapper::new([0,0,0],[0,0,255],max_iter);

                color_map.add_band_inv(128.0, [255, 0, 0]);
                color_map.add_band_inv(32.0, [0, 255, 255]);
                color_map.add_band_inv(8.0, [255, 255, 0]);
                color_map.add_band_inv(2.0, [0, 255, 0]);

                color_map
            }
        }
    }

    pub fn load_map(name: &str, max_iter: usize) -> ColorMapper {
        unimplemented!()
    }

    pub fn new(start: [u8; 3], end: [u8; 3], domain: usize) -> ColorMapper {
        ColorMapper {
            domain: domain,
            bands: vec![
                ColorBand {
                    index: 0,
                    color: start
                },
                ColorBand {
                    index: domain,
                    color: end
                }
            ],
            gradient_fn: 0
        }
    }

    pub fn add_band(&mut self, index: usize, color: [u8; 3]) {
        let cb = ColorBand {
            index: index,
            color: color
        };

        match self.bands.binary_search(&cb) {
            Ok(i) => self.bands[i].color = color,
            Err(i) => self.bands.insert(i, cb)
        }
    }

    pub fn add_band_f(&mut self, index: f32, color: [u8; 3]) {
        let domain = self.domain;
        self.add_band((domain as f32 * index) as usize, color)
    }

    pub fn add_band_inv(&mut self, index: f32, color: [u8; 3]) {
        self.add_band_f(1.0 / index, color)
    }

    fn find_bands(&self, index: usize) -> (ColorBand, ColorBand) {
        if index > self.domain {
            panic!("Highest color index is {}, tried to find color for {}",self.domain,index);
        }

        match self.bands.binary_search(&ColorBand {index: index, color: [0u8;3]}) {
            Ok(i) => (self.bands[i],self.bands[i]),
            Err(i) => (self.bands[i-1], self.bands[i])
        }
    }

    pub fn map(&self, index: usize) -> [u8; 3] {
        let (start, end) = self.find_bands(index);
        if start == end {
            start.color
        }  else {
            match self.gradient_fn {
                1 => map_color!(map_u8_s, start, end, index),
                _ => map_color!(map_u8, start, end, index)
            }
        }
    }
}

fn rgb_to_hsv(c: [u8; 3]) -> [u8; 3] {
    let r = c[0] as f32 / 255.0;
    let g = c[1] as f32 / 255.0;
    let b = c[2] as f32 / 255.0;

    let min = c.iter().min().unwrap().to_owned() as f32;
    let max = c.iter().max().unwrap().to_owned() as f32;

    let delta = max - min;
    let (h,s,v) = if delta > 0.00001 && max > 0.0 {
        let mut h = match max {
            x if x == r => (g-b)/delta,
            x if x == g => 2.0 + (b-r) / delta,
            _ => 4.0 + (r-g) / delta
        } / 6.0;

        if h < 0.0 {
            h+=1.0;
        }

        (h, delta/max, max)
    } else {
        (0.0,0.0,max)
    };

    [(h * 256.0) as u8, (s * 255.0) as u8, (v * 255.0) as u8]
}

fn hsv_to_rgb(c: [u8; 3]) -> [u8; 3] {
    let h = c[0] as f32 * 6.0 / 256.0;
    let s = c[1] as f32 / 255.0;
    let v = c[2] as f32 / 255.0;

    let (r,g,b) = if s == 0.0 {
        (v,v,v)
    } else {
        let region = h.trunc() as u8;
        let rem = h.fract();

        let p = v * (1.0 - s);
        let q = v * (1.0 - (s * rem));
        let t = v * (1.0 - (s * (1.0 - rem)));

        match region {
            0 => (v,t,p),
            1 => (q,v,p),
            2 => (p,v,t),
            3 => (p,q,v),
            4 => (t,p,v),
            _ => (v,p,q)
        }
    };

    [(r * 255.0) as u8, (g * 255.0) as u8, (b * 255.0) as u8]
}

