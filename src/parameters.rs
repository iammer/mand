extern crate getopts;

use num::traits::Num;
use self::getopts::Options;
use std::env;
use std::str::FromStr;
use std::fmt;

type Pair<T> = (T, T);
type Bounds<T> = (Pair<T>, Pair<T>);

fn get_opts() -> Result<getopts::Matches, String> {
    let args: Vec<String> = env::args().collect();
    let program = &args[0];

    let mut opts = Options::new();

    opts.optopt("c", "maxiter", "maximum iterations", "N");
    opts.optopt("z", "size", "dimension of image in pixels", "W,H");
    opts.optopt("s", "oversample", "Oversample (Anti-alias) pixels", "N");
    opts.optopt("b", "bounds", "area of fractal to show", "MIN_REAL,MIN_IMAG:MAX_REAL,MAX_IMAG");
    opts.optopt("p", "path", "zoom path", "N,N,...");
    opts.optopt("t", "threads", "num threads", "N");
    opts.optopt("o", "output", "image file to output", "FILE");
    opts.optopt("r", "preset", "color preset", "NAME");
    opts.optopt("m", "map", "color map", "FILE");
    opts.optopt("l", "bailout", "bailout method", "NUM");
    opts.optflag("q", "square", "use square pixels");
    opts.optflag("h","help","print this help menu");

    match opts.parse(&args[1..]) {
        Ok(ref matches) if matches.opt_present("h") => Err(opts.usage(&opts.short_usage(program))),
        Err(msg) => Err(format!("{}\n{}", msg, opts.usage(&opts.short_usage(program)))),
        Ok(x) => Ok(x) 
    }

}

pub fn get_params() -> Result<Parameters, String> {
    get_opts().and_then(Parameters::try_from)
}

#[inline]
fn split<T: Num + From<u8>>(min: Pair<T>, max: Pair<T>) -> Pair<T> {
    (
        (min.0 + max.0) / T::from(2),
        (min.1 + max.1) / T::from(2)
    )
}

pub struct Parameters {
    pub max_iter: u32,
    pub size: (u32, u32),
    pub min: (f64, f64),
    pub max: (f64, f64),
    pub thread_count: u8,
    pub file_name: String,
    pub oversample: f32,
    pub color_preset: String,
    pub color_map: Option<String>,
    pub bailout: u8
}

#[inline]
fn parse_num<T: FromStr>(s: &str) -> Result<T,String> {
    s.parse::<T>().map_err(|_| format!("Could not parse {} as number",s))
}

fn parse_pair<T: FromStr>(pair: &str) -> Result<Pair<T>, String> {
    let pair: Vec<&str> = pair.split(",").collect();
    if pair.len() == 2 {
        Ok((
            parse_num::<T>(pair[0])?,
            parse_num::<T>(pair[1])?
        ))
    } else {
        Err(format!("Expected pair {} to have two values",pair.join(",")))
    }
}

fn parse_bounds<T: FromStr>(bounds: &str) -> Result<Bounds<T>, String> {
    let bounds: Vec<&str> = bounds.split(":").collect();
    if bounds.len() == 2 {
        Ok((
            parse_pair::<T>(bounds[0])?,
            parse_pair::<T>(bounds[1])?
        ))
    } else {
        Err(format!("Expected bounds {} to have two parts",bounds.join(":")))
    }
}

fn or_default<T, F>(opt: Option<String>, default: T, f: F) -> Result<T, String> where F: FnOnce(&str) -> Result<T, String> {
    match opt {
        Some(s) => f(&s),
        None => Ok(default)
    }
}

fn follow_zoom_path(path: &str, (min, max): Bounds<f64>) -> Bounds<f64> {
    let mut min = min;
    let mut max = max;
    for z in path.split(',') {
        let mid = split(min, max);
        match z {
            "1" => max = mid,
            "2" => {
                min.0 = mid.0;
                max.1 = mid.1;
            },
            "3" => {
                max.0 = mid.0;
                min.1 = mid.1;
            },
            "4" => min = mid,
            _ => ()
        }
    }

    (min, max)
}

fn square_pixels(enabled: bool, size: Pair<u32>, (min, max): Bounds<f64>) -> Bounds<f64> {
    if enabled {
        let ratio = size.0 as f64 / size.1 as f64;        
        (min, ((max.1 - min.1) * ratio + min.0, max.1))
    } else {
        (min, max)
    }
}

impl Parameters {
    fn try_from(m: getopts::Matches) -> Result<Parameters, String> {

        let path = m.opt_str("path").unwrap_or(String::default());
        let size = or_default(m.opt_str("size"), (2048u32, 2048u32), parse_pair)?;
        let (min, max) = follow_zoom_path(
            &path,
            square_pixels(
                m.opt_present("square"),
                size,
                or_default(
                    m.opt_str("bounds"),
                    ((-1.4f64,-1.4f64),(1.4f64,1.4f64)),
                    |s| parse_bounds(s)
                )?
            )
        );

        Ok(
            Parameters {
                max_iter: or_default(m.opt_str("maxiter"), 2048u32, parse_num)?,
                size: size,
                oversample: or_default(m.opt_str("oversample"), 1f32, parse_num)?,
                min: min,
                max: max,
                thread_count: or_default(m.opt_str("threads"), 4, parse_num)?,
                file_name: m.opt_str("o").ok_or(String::from("No filename provided"))?,
                color_preset: m.opt_str("preset").unwrap_or(String::from("default")),
                color_map: m.opt_str("map"),
                bailout: or_default(m.opt_str("bailout"), 0u8, parse_num)?
            }
        )
    }
}

impl fmt::Display for Parameters {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "-c {} -z {},{} -b {},{}:{},{}", self.max_iter, self.size.0, self.size.1, self.min.0, self.min.1, self.max.0, self.max.1)
    }
}

