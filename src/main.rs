extern crate image;
extern crate num;
extern crate crossbeam;

use crossbeam::scope;
use image::GenericImage;
use num::complex::Complex;
use num::traits::Num;

mod scaler;
mod parameters;
mod colors;

use scaler::Scaler;
use parameters::get_params;
use colors::ColorMapper;


#[inline]
fn bailout(c: Complex<f64>, method: u8) -> bool {
    match method {
        1 => (c.re + c.im).abs() < 4.0,
        2 => c.norm_sqr() < 16.0,
        3 => c.re < 4.0 && c.im < 4.0,
        _ => (c.re.abs() + c.im.abs()) < 4.0
    }
}

#[inline]
fn get_slice_bound<T: Num + Copy + From<N> + From<M>, N: Num, M: Num>(y: T, i: N, t: M) -> (T,T) {
    let i = T::from(i);
    let t = T::from(t);

    (
        if i == T::zero() {
            T::zero()
        } else {
            y * i / t
        },
        y * (i + T::one()) / t
    )
}

fn main() {
    match get_params() {
        Ok(ref p) => {

            println!("{}",p);

            let size = (p.size.0 * p.oversample as u32, p.size.1 * p.oversample as u32);

            let mut buf = image::ImageBuffer::new(size.0, size.1);

            let color_map = &match p.color_map {
                Some(ref file) => ColorMapper::load_map(file, p.max_iter as usize),
                None =>  ColorMapper::preset(&p.color_preset, p.max_iter as usize)
            };

            scope(|scope| {
                for handle in (0..p.thread_count).map(|i: u8| {
                    scope.spawn(move || {
                        let (start, end) = get_slice_bound(size.1, i, p.thread_count);
                        let step = end - start;
                        let f_step = p.max.1 - p.min.1;
                        let (f_start, f_end) = get_slice_bound(f_step, i, p.thread_count);

                        let scaler = Scaler::new((size.0, step), (p.min.0, p.min.1 + f_start), (p.max.0, p.min.1 + f_end));

                        let bailout_method = p.bailout;

                        (start, image::ImageBuffer::from_fn(size.0, step, move |x,y| {
                            let c = scaler.scale_to_complex((x,y));

                            let mut z = c;
                            let mut i = 0;
                            while i < p.max_iter && bailout(z, bailout_method) {
                                z = z * z + c;
                                i+=1;
                            }

                            image::Rgb(color_map.map(i as usize))
                        }))
                    })
                }).collect::<Vec<_>>() {
                    let (start, sub_buf) = handle.join();
                    buf.copy_from(&sub_buf, 0, start);
                }
            });

            if p.oversample != 1.0 {
                buf = image::imageops::resize(&buf, p.size.0, p.size.1, image::FilterType::Lanczos3)
            }

            buf.save(&p.file_name).expect("Could not save image");
        },
        Err(msg) => println!("{}",msg)
    }
}
